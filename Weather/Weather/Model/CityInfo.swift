//
//  CityInfo.swift
//  Weather
//
//  Created by Veranika Shakh on 07/04/2022.
//

import Foundation

protocol CityInfo {
    var name: String? { get }
    var lat: Double { get }
    var lon: Double { get }
    var country: String? { get }
    var state: String? { get }
    var ru: String? { get }
    var en: String? { get }
    var locacalizedName: String? { get }
}

// MARK: - CityInfo

extension City: CityInfo {
    var ru: String? {
        self.localNames?.ru
    }

    var en: String? {
        self.localNames?.en
    }
}
