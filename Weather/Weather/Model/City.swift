//
//  City.swift
//  Weather
//
//  Created by Veranika Shakh on 16/02/2022.
//

import Foundation

struct City: Decodable {
    
    let name: String?
    let localNames: LocalNames?
    let lat: Double
    let lon: Double
    let country: String?
    let state: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case localNames = "local_names"
        case lat
        case lon
        case country
        case state
    }
    
    struct LocalNames: Decodable {
        let en: String?
        let ru: String?
    }
}

extension City {
    var locacalizedName: String? {
        
        guard let localNames = self.localNames else {
            return self.name
        }
        let lang = Locale.current.languageCode
        
        switch lang {
        case "ru":
            return localNames.ru ?? self.name
        case "en":
            return localNames.en ?? self.name
        default:
            return self.name
        }
    }
}
