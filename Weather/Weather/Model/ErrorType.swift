//
//  ErrorType.swift
//  Weather
//
//  Created by Veranika Shakh on 20/02/2022.
//

import Foundation

enum ErrorType: Error {
    case network
    case parse
    case common
}
