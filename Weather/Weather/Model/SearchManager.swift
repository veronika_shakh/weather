//
//  SearchManager.swift
//  Weather
//
//  Created by Veranika Shakh on 16/02/2022.
//

import Foundation

class SearchManager {
    
    // MARK: - Properties
    
    static let shared = SearchManager()
    private init() {}
    
    private let apiKey = "3f17ffe511a9b9aa0695c1c2fede24fb"
    private let limit = 5
    private var searchTask: URLSessionDataTask?
    
    
    // MARK: - Public func
    
    func search(by city: String, completion: @escaping ([City]?, ErrorType?) -> Void) {

        var components = URLComponents(string: "http://api.openweathermap.org/geo/1.0/direct")
        components?.queryItems = [
            URLQueryItem(name: "q", value: city),
            URLQueryItem(name: "limit", value: "\(limit)"),
            URLQueryItem(name: "apiKey", value: apiKey)
        ]
        guard let url = components?.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        searchTask?.cancel()
        searchTask = URLSession.shared.dataTask(with: request) { data, response, error in
            if error == nil,
               let data = data {
                do {
                    let cities = try JSONDecoder().decode([City].self, from: data)
                    DispatchQueue.main.async {
                        completion(cities, nil)
                    }
                } catch let parseError {
                    print(parseError)
                    DispatchQueue.main.async {
                        completion(nil, ErrorType.parse)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, ErrorType.common)
                }
            }
        }
        searchTask?.resume()
    }
    
    func search(by lat: Double, lon: Double, completion: @escaping ([City]?, ErrorType?) -> Void) {
        
        var components = URLComponents(string: "http://api.openweathermap.org/geo/1.0/reverse")
        components?.queryItems = [
            URLQueryItem(name: "lat", value: "\(lat)"),
            URLQueryItem(name: "lon", value: "\(lon)"),
            URLQueryItem(name: "limit", value: "\(limit)"),
            URLQueryItem(name: "apiKey", value: apiKey)
        ]
        guard let url = components?.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        searchTask?.cancel()
        searchTask = URLSession.shared.dataTask(with: request) { data, response, error in
            if error == nil,
               let data = data {
                do {
                    let cities = try JSONDecoder().decode([City].self, from: data)
                    DispatchQueue.main.async {
                        completion(cities, nil)
                    }
                } catch let error {
                    print(error)
                    DispatchQueue.main.async {
                        completion(nil, ErrorType.parse)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, ErrorType.common)
                }
            }
        }
        searchTask?.resume()
    }
}
