//
//  CityEntity+CoreDataProperties.swift
//  Weather
//
//  Created by Veranika Shakh on 07/03/2022.
//
//

import Foundation
import CoreData


extension CityEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CityEntity> {
        return NSFetchRequest<CityEntity>(entityName: "CityEntity")
    }

    @NSManaged public var name: String?
    @NSManaged public var lat: Double
    @NSManaged public var lon: Double
    @NSManaged public var country: String?
    @NSManaged public var state: String?
    @NSManaged public var localNames: LocalNamesEntity?

}

extension CityEntity : Identifiable {

}
