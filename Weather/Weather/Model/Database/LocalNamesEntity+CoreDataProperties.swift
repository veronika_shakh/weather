//
//  LocalNamesEntity+CoreDataProperties.swift
//  Weather
//
//  Created by Veranika Shakh on 07/03/2022.
//
//

import Foundation
import CoreData


extension LocalNamesEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LocalNamesEntity> {
        return NSFetchRequest<LocalNamesEntity>(entityName: "LocalNamesEntity")
    }

    @NSManaged public var en: String?
    @NSManaged public var ru: String?
    @NSManaged public var city: CityEntity?

}

extension LocalNamesEntity : Identifiable {

}
