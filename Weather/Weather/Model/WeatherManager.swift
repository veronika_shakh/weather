//
//  CityWeather.swift
//  Weather
//
//  Created by Veranika Shakh on 16/02/2022.
//

import Foundation
import UIKit

class WeatherManager {
    
    // MARK: - Properties
    
    static let shared = WeatherManager()
    private init() {}
    
    private let apiKey = "3f17ffe511a9b9aa0695c1c2fede24fb"
    private let units = "metric"
    private let exclude = "minutely,hourly,alerts"
    
    // MARK: - Public func
    
    
    func loadDailyWeather(for lat: Double, lon: Double, completion: @escaping (WeatherItem?, ErrorType?) -> Void) {
        
        var components = URLComponents(string: "https://api.openweathermap.org/data/2.5/onecall")
        components?.queryItems = [
            URLQueryItem(name: "lat", value: "\(lat)"),
            URLQueryItem(name: "lon", value: "\(lon)"),
            URLQueryItem(name: "apiKey", value: apiKey),
            URLQueryItem(name: "units", value: units),
            URLQueryItem(name: "exclude", value: exclude)
        ]
        guard let url = components?.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error == nil,
               let data = data {
                do {
                    let weather = try JSONDecoder().decode(WeatherItem.self, from: data)
                    DispatchQueue.main.async {
                        completion(weather, nil)
                    }
                } catch let parseError {
                    print(parseError)
                    DispatchQueue.main.async {
                        completion(nil, ErrorType.parse)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, ErrorType.common)
                }
            }
        }
        task.resume()
    }
    
    func loadWeatherIcon(for icon: String, completion: @escaping (UIImage?, ErrorType?) -> Void) {
        let urlString = "http://openweathermap.org/img/wn/\(icon)@2x.png"
        guard let url = URL(string: urlString) else { return }
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard
                error == nil,
                let data = data
            else {
                DispatchQueue.main.async {
                    completion(nil, .network)
                }
                return
            }

            let image = UIImage(data: data)
            DispatchQueue.main.async {
                completion(image, nil)
            }
        }
        task.resume()
    }
}

