//
//  Weather.swift
//  Weather
//
//  Created by Veranika Shakh on 23/02/2022.
//

import Foundation

struct WeatherItem: Decodable {
    
    let lat: Double
    let lon: Double
    let current: Current
    let daily: [Daily]
}

struct Current: Decodable {
    
    let dt: Double
    let temp: Double
    let humidity: Int
    let clouds: Int
    let windSpeed: Double
    let weather: [Weather]
    
    enum CodingKeys: String, CodingKey {
        case dt
        case temp
        case humidity
        case clouds
        case windSpeed = "wind_speed"
        case weather
    }
}

struct Weather: Decodable {
    
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct Daily: Decodable {
    
    let dt: Double
    let temp: Temp
    let weather: [WeatherDay]
}

struct Temp: Decodable {
    
    let day: Double
    let night: Double
}

struct WeatherDay: Decodable {
    
    let icon: String
}
