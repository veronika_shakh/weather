//
//  SearchViewModel.swift
//  Weather
//
//  Created by Veranika Shakh on 06/04/2022.
//

import Foundation
import UIKit
import CoreLocation

class SearchViewModel: NSObject {
    
    // MARK: - Properties
    
    private let locationManager = CLLocationManager()
    
    var searchCellViewModels = Bindable<[SearchCellViewModel]>([])
    var cities = Bindable<[City]>([])
    
    var title = Bindable<String?>(nil)
    var search = Bindable<String?>(nil)
    
    var isSearchInProgress = Bindable<Bool>(false)
    var error = Bindable<ErrorType?>(nil)
    
    override init() {
        super.init()
        title.value = "Weather".localized
        search.value = "Search for a city".localized
        
        setupLocation()
    }
    
    // MARK: - Public func
    
    func search(text: String) {
        isSearchInProgress.value = true
        SearchManager.shared.search(by: text) { [weak self] newCities, error in
            guard let self = self else { return }
            self.isSearchInProgress.value = false
            self.searchDidFinish(newCities, error)
        }
    }
    
    func searchByLocation() {
        guard let location = locationManager.location?.coordinate else { return }
        isSearchInProgress.value = true
        let longitude = location.longitude
        let latitude = location.latitude
        SearchManager.shared.search(by: latitude, lon: longitude) {  [weak self] newCities, error in
            self?.searchDidFinish(newCities, error)
        }
    }
  
    func cellsCount() -> Int {
        return searchCellViewModels.value.count
    }
    
    func cellViewModel(for indexPath: IndexPath) -> SearchCellViewModel {
        return searchCellViewModels.value[indexPath.row]
    }
    
    // MARK: - Private func
    
    private func searchDidFinish(_ newCities: [City]?, _ error: ErrorType?) {
        cities.value = newCities ?? []
        isSearchInProgress.value = false
        self.error.value = error
        var newViewModels: [SearchCellViewModel] = []
        cities.value.forEach { city in
            let viewModel = SearchCellViewModel(cityName: city.locacalizedName, country: city.country)
            newViewModels.append(viewModel)
        }
        searchCellViewModels.value = newViewModels
    }
    
    private func setupLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
}

// MARK: - CLLocationManagerDelegate

extension SearchViewModel: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard manager.location?.coordinate != nil else { return }
        
        self.locationManager.stopUpdatingLocation()
        self.searchByLocation()
    }
}
