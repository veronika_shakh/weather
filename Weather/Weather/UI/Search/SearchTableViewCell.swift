//
//  SearchTableViewCell.swift
//  Weather
//
//  Created by Veranika Shakh on 08/03/2022.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    // MARK: - Properties
    
    private var searchViewModel: SearchCellViewModel!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedBackgroundView = UIView.selectedBackgroundView()
    }
    
    // MARK: - Public func
    
    func update(with viewModel: SearchCellViewModel) {
        self.searchViewModel = viewModel
        bind()
    }
    
    // MARK: - Private func
    
    private func bind() {
        searchViewModel.cityName.bind { [weak self] name in
            self?.cityLabel.text = name
        }
        searchViewModel.country.bind { [weak self] country in
            self?.countryLabel.text = country
        }
    }
}
