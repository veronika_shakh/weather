//
//  SearchViewController.swift
//  Weather
//
//  Created by Veranika Shakh on 08/03/2022.
//

import UIKit
import CoreLocation

class SearchViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var citySearchBar: UISearchBar!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var backgraundImageView: UIImageView!
    
    // MARK: - Properties
    
    private let searchViewModel = SearchViewModel()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchTableView.keyboardDismissMode = .onDrag
        bind()
        backgraundImageView.addParallax()
    }
    
    // MARK: - Private func
    
    private func bind() {
        searchViewModel.title.bind { [weak self] title in
            self?.navigationItem.title = title
        }
        searchViewModel.search.bind { [weak self] search in
            self?.citySearchBar.placeholder = search
        }
        searchViewModel.isSearchInProgress.bind { [weak self] isSearchingProgress in
            guard let self = self else { return }
            if isSearchingProgress {
                self.loadingIndicator.startAnimating()
            } else {
                self.loadingIndicator.stopAnimating()
            }
        }
        searchViewModel.searchCellViewModels.bind { [weak self] test in
            self?.searchTableView.reloadData()
        }
        searchViewModel.error.bind { [weak self] error in
            guard
                let self = self,
                let searchManagerError = error
            else { return }
            
            switch searchManagerError {
            case .network:
                self.showAlertError(title: "ERROR", message: "No connection. Please try again.")
            case .parse:
                break
            case .common:
                break
            }
        }
    }
}

// MARK: - UITableViewDataSource

extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchViewModel.cellsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as? SearchTableViewCell
        else {
            return UITableViewCell()
        }
        cell.update(with: searchViewModel.cellViewModel(for: indexPath))
        return cell
    }
}

// MARK: - UITableViewDelegate

extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer {
            searchTableView.deselectRow(at: indexPath, animated: true)
        }
        guard
            let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "WeatherNavigationController") as? UINavigationController,
            let weatherVC = navigationController.viewControllers.first as? WeatherViewController
        else { return }
            
        let city = searchViewModel.cities.value[indexPath.row]
        let viewModel = WeatherViewModel(cityInfo: city)
        weatherVC.weatherViewModel = viewModel
        weatherVC.delegate = self
    
        present(navigationController, animated: true, completion: nil)
    }
}

// MARK: - UISearchBarDelegate

extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchViewModel.searchByLocation()
        } else {
            self.searchViewModel.search(text: searchText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        hideKeyboard()
        if let searchText = searchBar.text {
            self.searchViewModel.search(text: searchText)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - WeatherViewControllerDelegate

extension SearchViewController: WeatherViewControllerDelegate {
    func didAddCity() {
        dismiss(animated: true, completion: nil)
    }
}
