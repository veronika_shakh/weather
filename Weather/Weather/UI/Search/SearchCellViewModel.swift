//
//  SearchCellViewModel.swift
//  Weather
//
//  Created by Veranika Shakh on 06/04/2022.
//

import Foundation
import UIKit

class SearchCellViewModel {
  
    var cityName = Bindable<String?>(nil)
    var country = Bindable<String?>(nil)
    
    init(cityName: String?, country: String?) {
        self.cityName.value = cityName
        self.country.value = country
    }
}
