//
//  WeatherViewController.swift
//  Weather
//
//  Created by Veranika Shakh on 16/02/2022.
//

import UIKit

protocol WeatherViewControllerDelegate: AnyObject {
    func didAddCity()
}

class WeatherViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cloudinessLable: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var cloudinessTitleLabel: UILabel!
    @IBOutlet weak var humidityTitleLabel: UILabel!
    @IBOutlet weak var windTitleLabel: UILabel!
    
    @IBOutlet weak var weekWeatherTableView: UITableView!
    @IBOutlet weak var backgraundImageView: UIImageView!
    
    // MARK: - Properties
    
    var weatherViewModel: WeatherViewModel!
    weak var delegate: WeatherViewControllerDelegate?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherViewModel.loadWeather()
        bind()
        backgraundImageView.addParallax()
        
        if isModal {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized,
                                                               style: .plain,
                                                               target: self,
                                                               action: #selector(cancelButtonDidTap(_:)))
        } else {
            navigationItem.leftBarButtonItem = nil
        }
    }
    
    // MARK: - Action
    
    @IBAction func cancelButtonDidTap(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonDidTap(_ sender: UIBarButtonItem) {
        weatherViewModel.addCity()
        dismiss(animated: true, completion: nil)
        delegate?.didAddCity()
    }
    
    // MARK: - Private func
    
    private func bind() {
        weatherViewModel.cityName.bind { [weak self] cityName in
            self?.cityLabel.text = cityName
        }
        weatherViewModel.cloudinessTitle.bind { [weak self] cloudiness in
            self?.cloudinessTitleLabel.text = cloudiness
        }
        weatherViewModel.humidityTitle.bind { [weak self] humidity in
            self?.humidityTitleLabel.text = humidity
        }
        weatherViewModel.wind.bind { [weak self] wind in
            self?.windTitleLabel.text = wind
        }
        weatherViewModel.date.bind { [weak self] date in
            self?.dateLabel.text = date
        }
        weatherViewModel.temperature.bind { [weak self] temperature in
            self?.temperatureLabel.text = temperature
        }
        weatherViewModel.cloudiness.bind { [weak self] cloudiness in
            self?.cloudinessLable.text = cloudiness
        }
        weatherViewModel.humidity.bind { [weak self] humidity in
            self?.humidityLabel.text = humidity
        }
        weatherViewModel.wind.bind { [weak self] wind in
            self?.windLabel.text = wind
        }
        weatherViewModel.daily.bind { [weak self] _ in
            self?.weekWeatherTableView.reloadData()
        }
        
        weatherViewModel.isCityAdded.bind { [weak self] cityAdded in
            guard let self = self else { return }
            if cityAdded {
                self.navigationItem.rightBarButtonItem = nil
            } else {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "plus"),
                                                                         style: .plain,
                                                                         target: self,
                                                                         action: #selector(self.addButtonDidTap(_:)))
            }
        }
    }
}

// MARK: - UITableViewDataSource

extension WeatherViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherViewModel.cellsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell", for: indexPath) as? WeatherTableViewCell
        else {
            return UITableViewCell()
        }
        cell.update(with: weatherViewModel.cellViewModel(for: indexPath))
       return cell
    }
}
