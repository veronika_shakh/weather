//
//  WeatherViewModel.swift
//  Weather
//
//  Created by Veranika Shakh on 05/04/2022.
//

import Foundation
import UIKit
import CoreData

class WeatherViewModel {
    
    // MARK: - Properties
    
    var cityInfo: Bindable<CityInfo>
    var isCityAdded = Bindable<Bool>(true)
   
    var cloudinessTitle = Bindable<String?>(nil)
    var humidityTitle = Bindable<String?>(nil)
    var windTitle = Bindable<String?>(nil)
    
    var daily = Bindable<[WeatherCellViewModel]>([])

    var weather = Bindable<WeatherItem?>(nil)
    var date = Bindable<String?>(nil)
    var temperature = Bindable<String?>(nil)
    var cloudiness = Bindable<String?>(nil)
    var humidity = Bindable<String?>(nil)
    var wind = Bindable<String?>(nil)
    var cityName = Bindable<String?>(nil)

    init(cityInfo: CityInfo) {
        self.cloudinessTitle.value = "Cloudiness".localized
        self.humidityTitle.value = "Humidity".localized
        self.windTitle.value = "Wind".localized
        self.cityInfo =  Bindable<CityInfo>(cityInfo)
        self.cityName.value = cityInfo.locacalizedName
        
        checkIsCityAdded()
    }
    
    // MARK: - Public func
    
    func cellsCount() -> Int {
        return daily.value.count
    }
    
    func cellViewModel(for indexPath: IndexPath) -> WeatherCellViewModel {
        return daily.value[indexPath.row]
    }
    
    func addCity() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let addCity = CityEntity(context: context)
        addCity.name = cityInfo.value.name
        addCity.lon = cityInfo.value.lon
        addCity.lat = cityInfo.value.lat
        addCity.country = cityInfo.value.country
        addCity.state = cityInfo.value.state
        
        let newLocalNames = LocalNamesEntity(context: context)
        newLocalNames.en = cityInfo.value.en
        newLocalNames.ru = cityInfo.value.ru
        
        addCity.localNames = newLocalNames
        
        do {
            try context.save()
        } catch let error {
            print(error)
        }
    }
    
    func checkIsCityAdded() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        let getRequest = CityEntity.fetchRequest() as NSFetchRequest<CityEntity> 
      
        var predicates: [NSPredicate] = []
        
        if let cityName = cityInfo.value.name {
            let namePredicate = NSPredicate(format: "name == %@", cityName)
            predicates.append(namePredicate)
        }
        
        if let country = cityInfo.value.country {
            let countryPredicate = NSPredicate(format: "country == %@", country)
            predicates.append(countryPredicate)
        }
        
        let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: predicates)
        
        getRequest.predicate = finalPredicate
        getRequest.fetchLimit = 1
        
        do {
            let cityEntity = try context.fetch(getRequest).first
            let isCitySaved = (cityEntity != nil)
            self.isCityAdded.value = isCitySaved
        } catch let error {
            print(error)
        }
    }
    
    func loadWeather() {
        WeatherManager.shared.loadDailyWeather(for: cityInfo.value.lat, lon: cityInfo.value.lon) { weather, error in
            if let weather = weather {
                self.update(weather: weather)
            }
            
            if let error = error {
                print(error)
            }
        }
    }
    
    // MARK: - Private func
    
    private func update(weather: WeatherItem) {
        
        self.weather.value = weather
        let date = Date(timeIntervalSince1970: weather.current.dt)
        let dateFormatter = DateFormatter.dMMMMFormatter()
        let dateString = dateFormatter.string(from: date)
        self.date.value = dateString
        
        let temperature = Int(weather.current.temp)
        self.temperature.value = String(temperature) + "°"
        
        let cloudiness = Int(weather.current.clouds)
        self.cloudiness.value = String(cloudiness)
        
        let humidity = Int(weather.current.clouds)
        self.humidity.value = String(humidity)
        
        let wind = Int(weather.current.windSpeed)
        let speed = "meter/sec".localized
        self.wind.value = String(wind) + " " + speed
        
        var newViewModels: [WeatherCellViewModel] = []
        weather.daily.forEach { daily in
            let viewModel = WeatherCellViewModel(dailyWeather: daily)
            newViewModels.append(viewModel)
        }
        self.daily.value = newViewModels
    }
}

