//
//  WeatherTableViewCell.swift
//  Weather
//
//  Created by Veranika Shakh on 22/02/2022.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    
    @IBOutlet weak var weatherImageView: UIImageView!
   
    // MARK: - Properties
    
    private var weatherCellViewModel: WeatherCellViewModel!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedBackgroundView = UIView.selectedBackgroundView()
    }
    
    // MARK: - Public func
    
    func update(with viewModel: WeatherCellViewModel) {
        weatherCellViewModel = viewModel
        bind()
    }
    
    // MARK: - Private func
    
    private func bind() {
        weatherCellViewModel.day.bind { [weak self] day in
            self?.dayLabel.text = day
        }
        weatherCellViewModel.date.bind { [weak self] date in
            self?.dateLable.text = date
        }
        weatherCellViewModel.weather.bind { [weak self] weather in
            self?.weatherLabel.text = weather
        }
        weatherCellViewModel.image.bind { [weak self] image in
            self?.weatherImageView.image = image
        }
    }
}
