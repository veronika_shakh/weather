//
//  WeatherCellViewModel.swift
//  Weather
//
//  Created by Veranika Shakh on 04/04/2022.
//

import Foundation
import UIKit

class WeatherCellViewModel {
    
    var day = Bindable<String?>(nil)
    var date = Bindable<String?>(nil)
    var weather = Bindable<String?>(nil)
    var image = Bindable<UIImage?>(nil)
    
    init(dailyWeather: Daily) {
        let date = Date(timeIntervalSince1970: dailyWeather.dt)
        let dateString = DateFormatter.dMMMMFormatter().string(from: date)
        self.date.value = dateString
        
        let day = DateFormatter.eeeeFormatter().string(from: date)
        self.day.value = day
        
        let temperature = Int(dailyWeather.temp.day)
        self.weather.value = "\(temperature)°"
        
        if let icon = dailyWeather.weather.first?.icon {
            WeatherManager.shared.loadWeatherIcon(for: icon) { [weak self] image, error in
                self?.image.value = image
            }
        }
    }
}
