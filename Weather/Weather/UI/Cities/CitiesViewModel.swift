//
//  CitiesViewModel.swift
//  Weather
//
//  Created by Veranika Shakh on 04/04/2022.
//

import Foundation
import UIKit
import CoreData

class CitiesViewModel {
    
    var cityCellViewModels = Bindable<[CityCellViewModel]>([])
    var cityEntities = Bindable<[CityEntity]>([])
   
    var title = Bindable<String?>(nil)
    var search = Bindable<String?>(nil)
    
    init() {
        self.title.value = "Weather".localized
        self.search.value = "Search for a city".localized
    }
    
    func reloadData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        let getRequest = CityEntity.fetchRequest() as NSFetchRequest<CityEntity>
        do {
            let cityEntities = try context.fetch(getRequest)
            
            var newViewModels: [CityCellViewModel] = []
            cityEntities.forEach { cityEntity in
                let viewModel = CityCellViewModel(name: cityEntity.name,
                                                  country: cityEntity.state,
                                                  lat: cityEntity.lat,
                                                  lon: cityEntity.lon)
                newViewModels.append(viewModel)
            }
            
            self.cityEntities.value = cityEntities
            self.cityCellViewModels.value = newViewModels
            
        } catch let error {
            print(error)
        }
    }
    
    func cellsCount() -> Int {
        return cityCellViewModels.value.count
    }
    
    func cellViewModel(for indexPath: IndexPath) -> CityCellViewModel {
        return cityCellViewModels.value[indexPath.row]
    }
    
    func deleteCity(at indexPath: IndexPath) {
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            cityEntities.value.count > indexPath.row
        else { return }
        
        let city = self.cityEntities.value[indexPath.row]
        let context = appDelegate.persistentContainer.viewContext
        context.delete(city)
        do {
            try context.save()
        } catch let error {
            print(error)
        }
        cityEntities.value.remove(at: indexPath.row)
        cityCellViewModels.value.remove(at: indexPath.row)
    }
}
