//
//  CityTableViewCell.swift
//  Weather
//
//  Created by Veranika Shakh on 16/02/2022.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var weatherLable: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    // MARK: - Properties
    
    private var cityCellViewModel: CityCellViewModel!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedBackgroundView = UIView.selectedBackgroundView()
    }
    
    // MARK: - Public func
    
    func update(with viewModel: CityCellViewModel) {
        cityCellViewModel = viewModel
        bind()
    }
    
    // MARK: - Private func
    
    private func bind() {
        cityCellViewModel.name.bind { [weak self] city in
            self?.cityLabel.text = city
        }
        cityCellViewModel.country.bind { [weak self] country in
            self?.countryLabel.text = country
        }
        cityCellViewModel.weatherImage.bind { [weak self] weatherImage in
            self?.weatherImageView.image = weatherImage
        }
        cityCellViewModel.temp.bind { [weak self] weather in
            self?.weatherLable.text = weather
        }
    }
}
