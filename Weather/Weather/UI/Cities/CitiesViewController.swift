//
//  CitiesViewController.swift
//  Weather
//
//  Created by Veranika Shakh on 16/02/2022.
//

import UIKit
import CoreLocation
import CoreData

class CitiesViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var searchCityBar: UISearchBar!
    @IBOutlet weak var citiesTableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var backgraundImageView: UIImageView!
    
    // MARK: - Properties
    
    private let citiesViewModel = CitiesViewModel()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
        citiesTableView.keyboardDismissMode = .onDrag
        backgraundImageView.addParallax()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        citiesViewModel.reloadData()
    }
    
    // MARK: - Private func
    
    private func bind() {
        citiesViewModel.title.bind { [weak self] title in
            self?.navigationItem.title = title
        }
        citiesViewModel.search.bind { [weak self] search in
            self?.searchCityBar.placeholder = search
        }
        citiesViewModel.cityCellViewModels.bind { [weak self] viewModels in
            guard
                let self = self,
                !self.citiesTableView.isEditing
            else { return }
            self.citiesTableView.reloadData()
        }
    }
}

// MARK: - UITableViewDataSource

extension CitiesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citiesViewModel.cellsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as? CityTableViewCell
        else {
            return UITableViewCell()
        }
        cell.update(with: citiesViewModel.cellViewModel(for: indexPath))
        return cell
    }
}

// MARK: - UITableViewDelegate

extension CitiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer {
            citiesTableView.deselectRow(at: indexPath, animated: true)
        }
        guard
            let weatherVC = self.storyboard?.instantiateViewController(withIdentifier: "WeatherViewController") as? WeatherViewController
        else { return }
            
        let city = citiesViewModel.cityEntities.value[indexPath.row]
        let viewModel = WeatherViewModel(cityInfo: city)
        weatherVC.weatherViewModel = viewModel
          
        navigationController?.pushViewController(weatherVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView,
                   titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        let delete = "Delete".localized
        return delete
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            citiesViewModel.deleteCity(at: indexPath)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

// MARK: - UISearchBarDelegate

extension CitiesViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        if let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "SearchNavigationController") as? UINavigationController {
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        hideKeyboard()
        citiesViewModel.reloadData()
    }
}

// MARK: - CityInfo

extension CityEntity: CityInfo {
    var locacalizedName: String? {
        guard let localNames = self.localNames else {
            return self.name
        }
        let lang = Locale.current.languageCode
        
        switch lang {
        case "ru":
            return localNames.ru ?? self.name
        case "en":
            return localNames.en ?? self.name
        default:
            return self.name
        }
    }
    
    var ru: String? {
        localNames?.ru
    }
    
    var en: String? {
        localNames?.en
    }
}
