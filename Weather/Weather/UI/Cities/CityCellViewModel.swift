//
//  CityCellViewModel.swift
//  Weather
//
//  Created by Veranika Shakh on 04/04/2022.
//

import Foundation
import UIKit

class CityCellViewModel {
    
    var name = Bindable<String?>(nil)
    var country = Bindable<String?>(nil)
    var temp = Bindable<String?>(nil)
    var weatherImage = Bindable<UIImage?>(nil)
    var weatherItem = Bindable<WeatherItem?>(nil)
    
    private let lat: Double
    private let lon: Double
    
    init(name: String?, country: String?, lat: Double,  lon: Double) {
        self.name.value = name
        self.country.value = country
        self.lat = lat
        self.lon = lon
        loadWeather()
    }

    func loadWeather() {
        WeatherManager.shared.loadDailyWeather(for: lat, lon: lon) { [weak self] weather, _ in
            guard
                let self = self,
                let weather = weather
            else { return }
            
            let temp = Int(weather.current.temp)
            self.temp.value = String(temp) + "°"
            
            if let icon = weather.current.weather.first?.icon {
                WeatherManager.shared.loadWeatherIcon(for: icon) { [weak self] image, error in
                    self?.weatherImage.value = image
                }
            }
        }
    }
}
