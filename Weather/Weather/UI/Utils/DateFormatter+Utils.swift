//
//  DateFormatter+Utils.swift
//  Weather
//
//  Created by Veranika Shakh on 26/05/2022.
//

import Foundation

extension DateFormatter {
    
    static func dMMMMFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM"
        return dateFormatter
    }
    
    static func eeeeFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter
    }
    
}
