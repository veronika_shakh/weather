//
//  UIView+Utils.swift
//  Weather
//
//  Created by Veranika Shakh on 27/05/2022.
//

import Foundation
import UIKit

// MARK: - Parallax

extension UIView {
    
    func addParallax(shift: Int = 40) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -shift
        horizontal.maximumRelativeValue = shift
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -shift
        vertical.maximumRelativeValue = shift
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        addMotionEffect(group)
    }
}

// MARK: - Background Selection

extension UIView {
    
    static func selectedBackgroundView() -> UIView {
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        return backgroundView
    }
}
