//
//  String+Utils.swift
//  Weather
//
//  Created by Veranika Shakh on 06/03/2022.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
