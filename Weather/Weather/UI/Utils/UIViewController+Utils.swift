//
//  UIViewController+Utils.swift
//  Weather
//
//  Created by Veranika Shakh on 17/02/2022.
//

import UIKit

// MARK: - Keyboard utils

extension UIViewController {
    func hideKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - Alert

extension UIViewController {
    func showAlertError(title: String, message: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK".localized, style: .default) { _ in }
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Presentation

extension UIViewController {

    var isModal: Bool {

        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController

        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
}
